<?php
namespace Magenest\TestFresher\Block\Widget;
use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\ProductFactory;


class ProductSlider extends Template implements \Magento\Widget\Block\BlockInterface {

    private $collectionFactory;
    private $productFactory;
    private $productRepositoryInterfaceFactory;
    private $imageHelper;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $collectionFactory,
        ProductFactory $productFactory,
        \Magento\Catalog\Api\ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory,
        \Magento\Catalog\Helper\Image $imageHelper
    )
    {
        $this->imageHelper = $imageHelper;
        $this->productRepositoryInterfaceFactory = $productRepositoryInterfaceFactory;
        $this->productFactory = $productFactory;
        $this->collectionFactory= $collectionFactory;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Magenest_TestFresher::widget/productslider.phtml');
    }

    public function getProducts(){
        $product = $this->collectionFactory->create();
        $product = $product->addFieldToSelect("*")->getData();
        return $product;
    }

    public function getPriceAndImage($id){
        $product = $this->productRepositoryInterfaceFactory->create()->getById($id);
        $price = $product->getData('price');
        $image = $this->imageHelper->init($product,'product_base_image')->getUrl();
        return ["image"=>$image,"price"=>$price];
    }

}
